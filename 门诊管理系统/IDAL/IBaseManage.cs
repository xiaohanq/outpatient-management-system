﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutDepModel;

namespace IDAL
{
    public interface IBaseManage<T> where T:Basemodel
    {
        T First(int id);

        List<T> Select();

        bool Insert(T Poc);

        bool Update(T Poc);

        bool Dleate(T Poc);

        bool Details(T Poc);

    }
}
