﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepViewModel
{
    public class OutdepSelectModel
    {
        [Display(Name = "医护编号")]
        public int SubId { get; set; }
        [Display(Name = "医护姓名")]
        public string Name { get; set; }
        [Display(Name = "年龄")]
        public int Age { get; set; }
        [Display(Name = "性别")]
        public bool Sex { get; set; } = true;
        [Display(Name = "科室")]
        public string Office { get; set; }
        [Display(Name = "入职日期")]
        public DateTime Time { get; set; }
        [Display(Name = "职务")]
        public string Post { get; set; }
        public bool Jobs { get; set; }
        [Display(Name = "是否离职")]
        public string Jobs1
        {
            get
            {
                if (Jobs == true)
                {
                    return "是";
                }
                return "否";
            }
        }
    }
    public class OutdepadCreateModel
    {
        [Display(Name = "医护姓名")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Name { get; set; }
        [Display(Name = "年龄")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Age { get; set; }
        [Display(Name = "性别")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public bool Sex { get; set; } = true;
        [Display(Name = "科室")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Office { get; set; }
        [Display(Name = "入职日期")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public DateTime Time { get; set; }
        [Display(Name = "职务")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Post { get; set; }
        [Display(Name = "是否离职")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public bool Jobs { get; set; }
    }
    public class OutdepEditMoodel
    {
        [Display(Name = "医护姓名")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Name { get; set; }
        [Display(Name = "年龄")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Age { get; set; }
        [Display(Name = "性别")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public bool Sex { get; set; } = true;
        [Display(Name = "科室")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Office { get; set; }
        [Display(Name = "职务")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Post { get; set; }
        [Display(Name = "是否离职")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public bool Jobs { get; set; }
    }
    public class OutdepDeleteModel
    {
        public int SubId { get; set; }
    }
}
