﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepViewModel
{
   public class OutDepInsertModel
    {
        /// <summary>
        /// 用于登录
        /// </summary>
        [Display(Name ="名称：")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Name { get; set; }
        [Display(Name = "密码：")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Pwd { get; set; }
        [Display(Name = "级别")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Rool { get; set; }
    }
    /// <summary>
    /// 查询
    /// </summary>
    public class OutDepSelectModel
    {
        /// <summary>
        /// 用于登录
        /// </summary>
        [Display(Name = "名称：")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Name { get; set; }
        [Display(Name = "密码：")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Pwd { get; set; }
        [Display(Name = "级别")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Rool { get; set; }
    }
}
