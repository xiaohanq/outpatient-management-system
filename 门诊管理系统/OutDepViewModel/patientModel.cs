﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepViewModel
{
    /// <summary>
    /// 显示列表
    /// </summary>
    public class patientselectModel
    {
        [Display(Name = "编号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Id { get; set; }
        [Display(Name = "患者姓名")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Name { get; set; }
        [Display(Name = "性别")]
        [Required(ErrorMessage = "{0} 不能为空")]
        //性别
        public bool Sex { get; set; } = true;
        [Display(Name = "性别")]
        [Required(ErrorMessage = "{0} 不能为空")]
        //性别
        public string Sex1 {
            get {
                if (Sex == true)
                {
                    return "男";
                } return "女";
            }
        }
        [Display(Name = "患者年龄")]
        [Required(ErrorMessage = "{0} 不能为空")]
        //年龄
        public int Age { get; set; }
        [Display(Name = "就诊状态")]
        [Required(ErrorMessage = "{0} 不能为空")]
        //判断状态
        public bool State { get; set; } = true;
        [Display(Name = "就诊状态")]
        [Required(ErrorMessage = "{0} 不能为空")]
        //判断状态
        public string State1
        {
            get
            {
                if (State == true)
                {
                    return "候诊";
                }
                return "就诊中";
            }
        }
        [Display(Name = "是否取药")]
        [Required(ErrorMessage = "{0} 不能为空")]
        //拿药状态
        public bool Medstate { get; set; } = true;
        //拿药状态
        public string Medstate1 { get {
                if (Medstate == true)
                {
                    return "未取药";
                } return "已取药";
            } }
        [Display(Name = "地址")]
        [Required(ErrorMessage = "{0} 不能为空")]
        //地址
        public string Address { get; set; }
        [Display(Name = "病因")]
        [Required(ErrorMessage = "{0} 不能为空")]
        //症状描述
        public string Symptom { get; set; }
        [Display(Name = "处方")]
        [Required(ErrorMessage = "{0} 不能为空")]
        //所需药品
        public string Ndmed { get; set; }
    }
    public class patientCreateModel
    {
         [Display(Name = "患者姓名")]
         [Required(ErrorMessage = "请输入{0}")]
        public string Name { get; set; }
        [Display(Name = "性别")]
        [Required(ErrorMessage = "请输入{0}")]
        //性别
        public bool Sex { get; set; } = true;
       
       
        [Display(Name = "患者年龄")]
        [Required(ErrorMessage = "请输入{0}")]
        //年龄
        public int Age { get; set; }
        [Display(Name = "就诊状态")]
        //判断状态
        public bool State { get; set; } = true;
        
        [Display(Name = "是否取药")]
        //拿药状态
        public bool Medstate { get; set; } = true;
      
        [Display(Name = "地址")]
        [Required(ErrorMessage = "请输入{0}")]
        //地址
        public string Address { get; set; }
        [Display(Name = "病因")]
        //症状描述
        public string Symptom { get; set; }
        [Display(Name = "处方")]
        //所需药品
        public string Ndmed { get; set; }

        [Display(Name ="医生")]
        public string OutDepName { get; set; }
    }
    /// <summary>
    /// 修改
    /// </summary>
    public class patientEditModel
    {
        [Display(Name = "患者姓名")]
        [Required(ErrorMessage = "请输入{0}")]
        public string Name { get; set; }
        [Display(Name = "性别")]

        //性别
        public bool Sex { get; set; } = true;
     
        [Display(Name = "患者年龄")]
        [Required(ErrorMessage = "请输入{0}")]
        //年龄
        public int Age { get; set; }
        [Display(Name = "就诊状态")]
        //判断状态
        public bool State { get; set; } = true;
       
        [Display(Name = "是否取药")]
        //拿药状态
        public bool Medstate { get; set; } = true;
      
        [Display(Name = "地址")]
        [Required(ErrorMessage = "请输入{0}")]
        //地址
        public string Address { get; set; }
        [Display(Name = "病因")]
        //症状描述
        public string Symptom { get; set; }
        [Display(Name = "处方")]
        //所需药品
        public string Ndmed { get; set; }

        [Display(Name = "医生")]
        public string OutDepName { get; set; }
    }
    /// <summary>
    /// 删除
    /// </summary>
    public class patientDeleteModel
{
        public int Id { get; set; }
}
}
