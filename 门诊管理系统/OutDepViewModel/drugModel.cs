﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepViewModel
{
    /// <summary>
    /// 用于查询
    /// </summary>
   public class drugSelectModel
    {
        public int Id { get; set; }
        [Display(Name = "药名")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Name { get; set; }
        [Display(Name = "规格")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Specification { get; set; }
        [Display(Name = "单位")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Unit { get; set; }
        [Display(Name = "单价")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Price { get; set; }
        [Display(Name = "生产日期")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public DateTime Prodtime { get; set; }
        [Display(Name = "有效日期")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public DateTime Usetime { get; set; }
        [Display(Name = "库存")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Inventory { get; set; }
        [Display(Name = "供应商编号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Supplier { get; set; }
        public bool Rxdrug { get; set; }
        [Display(Name = "处方药")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Rxdrug1
        {
            get
            {
                if (Rxdrug == true)
                {
                    return "是";
                }
                return "否";
            }
        }
        public bool shenpi { get; set; }
        [Display(Name = "审批状态")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string shenpi2
        {
            get
            {
                if (shenpi == true)
                {
                    return "已审批";
                }
                return "未审批";
            }
        }
    }
    /// <summary>
    /// 用于添加
    /// </summary>
    public class drugCreateModel
    {
        [Display(Name = "药名")]
        [Required(ErrorMessage ="{0} 不能为空")]
        public string Name { get; set; }
        [Display(Name = "规格")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Specification { get; set; }
        [Display(Name = "单位")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Unit { get; set; }
        [Display(Name = "单价")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Price { get; set; }
        [Display(Name = "生产日期")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public DateTime Prodtime { get; set; }
        [Display(Name = "有效日期")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public DateTime Usetime { get; set; }
        [Display(Name = "库存")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Inventory { get; set; }
        [Display(Name = "供应商编号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Supplier { get; set; }
        [Display(Name = "处方药")]
        [Required(ErrorMessage = "{0} 不能为空")]

        public bool Rxdrug { get; set; }
        [Display(Name = "审批状态")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public bool shenpi { get; set; } 
        [Display(Name = "审批状态")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string shenpi2
        {
            get
            {
                if (shenpi == true)
                {
                    return "已审批";
                }
                return "未审批";
            }
        }

    }
    /// <summary>
    /// 用于修改
    /// </summary>
    public class drugEditModel
    {
        
        [Display(Name = "药名")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Name { get; set; }
        [Display(Name = "单价")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Price { get; set; }
        [Display(Name = "库存")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Inventory { get; set; }
        [Display(Name = "处方药")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public bool Rxdrug { get; set; }

    }
    /// <summary>
    /// 用于删除
    /// </summary>
    public class drugDelteModel
    {
        public int Id { get; set; }
    }
}
