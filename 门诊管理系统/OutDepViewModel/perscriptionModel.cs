﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepViewModel
{
    /// <summary>
    /// 显示列表
    /// </summary>
    public class perscriptionSelectModel
    {
        //编号
        [Display(Name ="药品编号")]
        public int Id { get; set; }
        //药名
        [Display(Name ="名称")]
        public string Name { get; set; }
        //售价
        [Display(Name ="单价")]
        public int Price { get; set; }
        //数量
        [Display(Name ="数量")]
        public int Num { get; set; }
        //总价
        [Display(Name ="总计")]
        public int Sumprice { get; set; }
    }
    /// <summary>
    /// 添加
    /// </summary>
    public class perscriptionCreateModel
    {
        //药名
        [Display(Name = "名称")]
        [Required(ErrorMessage ="请输入{0}")]
        public string Name { get; set; }
        //售价
        [Display(Name = "单价")]
        [Required(ErrorMessage ="请输入{0}")]
        public int Price { get; set; }
        //数量
        [Display(Name = "数量")]
        [Required(ErrorMessage ="请输入{0}")]
        public int Num { get; set; }
        //总价
        [Display(Name = "总计")]
        [Required(ErrorMessage ="请输入{0}")]
        public int Sumprice { get; set; }
    }
    /// <summary>
    /// 修改
    /// </summary>
    public class perscriptionEditModel
    {
    
        //药名
        [Display(Name = "名称")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Name { get; set; }
        //售价
        [Display(Name = "单价")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Price { get; set; }
        //数量
        [Display(Name = "数量")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Num { get; set; }
        //总价
        [Display(Name = "总计")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public int Sumprice { get; set; }
    }
    public class perscriptionDeleteModel
    {
        //编号
        public int Id { get; set; }
    }
}
