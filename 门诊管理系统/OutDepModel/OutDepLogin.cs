﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepModel
{
    /// <summary>
    /// 账号密码表
    /// </summary>
    public class OutDepLogin : Basemodel
    {

        public string Name { get; set; }

        public string Pwd { get; set; }

        public int Rool { get; set; }
    }
}
