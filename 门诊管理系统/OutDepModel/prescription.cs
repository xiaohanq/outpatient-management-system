﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepModel
{
    public class Prescription : Basemodel
    {
        //药名
        public string Name { get; set; }
        //售价
        public int Price { get; set; }
        //数量
        public int Num { get; set; }
        //总价
        public int Sumprice { get; set; }
    }
}
