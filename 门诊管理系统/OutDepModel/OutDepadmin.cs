﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepModel
{
    /// <summary>
    /// 医护表
    /// </summary>
    public class OutDepadmin : Basemodel
    {

        //医护编号
        public int SubId { get; set; }

        //医护姓名
        public string Name { get; set; }

        public int Age { get; set; }

        public bool Sex { get; set; } = true;

        //科室
        public string Office { get; set; }

        //入职日期
        public DateTime Time { get; set; }

        //职务
        public string Post { get; set; }

        //是否离职
        public bool Jobs { get; set; } 



    }
}
