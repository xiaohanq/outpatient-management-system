﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepModel
{
    public class Patient:Basemodel
    {
        //姓名
        public string Name { get; set; }
        //性别
        public bool Sex { get; set; }

        //年龄
        public int Age{ get; set; }
        //判断状态
        public bool State{ get; set; }
        //拿药状态
        public bool Medstate { get; set; }
        //地址
        public string  Address { get; set; }
        //症状描述
        public string Symptom { get; set; }
        //所需药品
        public string Ndmed { get; set; }

        public string  OutDepName { get; set; }
    }
}
