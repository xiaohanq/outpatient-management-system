﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepModel
{
    public class Basemodel
    {
        public int Id { get; set; }

        public bool Isdeleted { get; set; } = false;
    }
}
