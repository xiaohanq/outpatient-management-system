﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutDepModel
{
    public class Drug : Basemodel
    {
        //药名
        public string  Name{ get; set; }
        //规格
        public string Specification { get; set; }
        //单位
        public string Unit { get; set; }
        //单价
        public int Price { get; set; }
        //生产日期
        public DateTime Prodtime { get; set; }
        //有效日期
        public DateTime Usetime { get; set; }
        //库存
        public int Inventory { get; set; }
        //供应商编号
        public int Supplier { get; set; }

        //审批状态
        public bool shenpi { get; set; } 
        //是否为处方药
        public bool Rxdrug { get; set; }
    }
}
