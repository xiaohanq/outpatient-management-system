﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="LoginWebs.Login" %>

<!DOCTYPE html>
<html  lang="zh-CN">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Content/script.js"></script>
    <script src="Content/gsap.min.js"></script>
    <link href="Content/style.css" rel="stylesheet" />
<title>医院登录</title>

<link rel="stylesheet" type="text/css" href="/css/styles.css">

<style>@media print {#ghostery-tracker-tally {display:none !important}}
</style></head>
<body>
    <div class="container container-fluid">


<div class="wrapper">

	<div class="container">
		<h1><a>
            <img src="Img/经开区人民医院.png" height="80" width="80"/></a>
            
            祝您早日康复</h1>
        
		<form class="form">
			<input type="text" placeholder="Username">
			<input type="password" placeholder="Password">
			<button type="submit" id="login-button">Login</button>

		</form>
	</div>




	
	<ul class="bg-bubbles">
		<li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="30" width="30"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="50" width="50"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="20" width="20"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="60" width="60"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="30" width="30"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="60" width="60"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="30" width="30"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="40" width="40"/></li>
        <li><img src="Img/li-医院图标.png"  height="30" width="30"/></li>

	</ul>
	
</div>
        
</div>

<script type="text/javascript" src="Scripts/jquery-2.1.1.min.js">
    <$('#login-button').click(function(event){
/script>
<script type="text/javascript">
	event.preventDefault();
	$('form').fadeOut(500);
	$('.wrapper').addClass('form-success');
});
</script>


</body>

</html>

