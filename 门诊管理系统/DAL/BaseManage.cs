﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDAL;
using OutDepModel;
using SQLserver;

namespace DAL
{
    public class BaseManage<T> : IBaseManage<T> where T : Basemodel
    {
        protected Model1 model = new Model1();
        bool IBaseManage<T>.Details(T Poc)
        {
            model.Set<T>().Attach(Poc);
            model.Entry<T>(Poc).State = System.Data.Entity.EntityState.Modified;
            return model.SaveChanges() > 0;

        }

        bool IBaseManage<T>.Dleate(T info)
        {
            T temp = model.Set<T>().Find(info.Id);
            if (temp != null)
            {
                temp.Isdeleted = true;
                model.Set<T>().Attach(temp);
                model.Entry<T>(temp).State = System.Data.Entity.EntityState.Modified;
                return model.SaveChanges() > 0;
            }
            return false;


        }

        T IBaseManage<T>.First(int id)
        {
            return model.Set<T>().FirstOrDefault(p => p.Id == id);
        }

        bool IBaseManage<T>.Insert(T Poc)
        {
            model.Set<T>().Add(Poc);
            return model.SaveChanges() > 0;
        }

        List<T> IBaseManage<T>.Select()
        {
            return model.Set<T>().Where(p => p.Isdeleted == false).ToList();
        }

        bool IBaseManage<T>.Update(T Poc)
        {
            model.Set<T>().Attach(Poc);
            model.Entry<T>(Poc).State = System.Data.Entity.EntityState.Modified;
            return model.SaveChanges() > 0;
        }
    }
}
