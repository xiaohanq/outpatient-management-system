﻿using AutoMapper;
using OutDepModel;
using OutDepViewModel;


namespace 门诊管理系统
{
    public class Automapper
    {
        public static IMapper Configmapper()
        {
            var config = new MapperConfiguration(
            p =>
            {
                p.CreateMap<patientselectModel, Patient>();
                p.CreateMap<Patient,patientselectModel >();
                p.CreateMap<patientCreateModel, Patient>();
                p.CreateMap<Patient,patientCreateModel >();
                p.CreateMap<patientEditModel, Patient>();
                p.CreateMap< Patient,patientEditModel>();
                p.CreateMap<patientDeleteModel, Patient>();
                p.CreateMap<Patient,patientDeleteModel >();
                p.CreateMap<perscriptionSelectModel, Prescription>();
                p.CreateMap< Prescription,perscriptionSelectModel>();
                p.CreateMap<Prescription, perscriptionCreateModel>();
                p.CreateMap< perscriptionCreateModel,Prescription>();
                p.CreateMap<Prescription, perscriptionEditModel>();
                p.CreateMap<perscriptionEditModel,Prescription >();
                p.CreateMap<Prescription,perscriptionDeleteModel >();
                p.CreateMap<perscriptionDeleteModel,Prescription >();

                p.CreateMap<Drug,drugSelectModel>();
                p.CreateMap<drugSelectModel,Drug>();
                p.CreateMap<drugCreateModel,Drug>();
                p.CreateMap<Drug,drugCreateModel>();
                p.CreateMap<drugEditModel,Drug>();
                p.CreateMap<Drug,drugEditModel>();
                p.CreateMap<drugDelteModel,Drug>();
                p.CreateMap<Drug,drugDelteModel>();
                p.CreateMap<OutdepSelectModel,OutDepadmin>();
                p.CreateMap<OutDepadmin,OutdepSelectModel>();
                p.CreateMap<OutdepadCreateModel,OutDepadmin>();
                p.CreateMap<OutDepadmin,OutdepadCreateModel>();
                p.CreateMap<OutdepEditMoodel, OutDepadmin>();
                p.CreateMap<OutDepadmin,OutdepEditMoodel >();
                p.CreateMap<OutdepDeleteModel, OutDepadmin>();
                p.CreateMap<OutDepadmin,OutdepDeleteModel >();
                p.CreateMap<OutDepInsertModel,OutDepLogin >();
                p.CreateMap<OutDepLogin,OutDepInsertModel >();
                p.CreateMap<OutDepSelectModel, OutDepLogin>();
                p.CreateMap<OutDepLogin,OutDepSelectModel >();

            });
            return config.CreateMapper();
        }
       

    }
}