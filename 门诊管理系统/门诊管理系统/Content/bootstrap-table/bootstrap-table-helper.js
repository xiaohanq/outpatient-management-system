﻿(function ($) {
    var defaults = {
        target: ".table",
        method: 'get',                      //请求方式（*）
        uniqueId: "id",                     //每一行的唯一标识，一般为主键列 
        dataType: "json",

        sortable: true,                     //是否启用排序
        sortName: 'createTime',                     // 设置默认排序为 Id
        sortOrder: 'desc',                   // 设置排序为反序 desc
        bsColumn: {
            select: {
                show: true,
                column: {
                    clickToSelect: false,
                    checkbox: true,
                    sortable: false,
                    align: "center"
                }
            },
            operate: {
                show: true,
                column: {
                    title: '操作',
                    clickToSelect: false,
                    sortable: false,
                    align: "center",
                    buttons: {
                        delete: {
                            show: true,
                            href: "javascript:void(0)",
                            class: "text-danger",
                            icon: "fa fa-trash",
                            text: "",
                            title: "删除"
                        },
                        update: {
                            show: true,
                            href: "javascript:void(0)",
                            class: "text-primary",
                            icon: "fa fa-edit",
                            text: "",
                            title: "修改"
                        },
                        detail: {
                            show: true,
                            href: "javascript:void(0)",
                            class: "text-primary",
                            icon: "fa fa-file-text",
                            text: "",
                            title: "详情"
                        },
                        append: ""
                    }
                }
            }
        },
        bsToolbar: {
            target: ".bs-table-toolbar",
            /**
             * 工具事件处理函数
             * @param {object} $sender 事件源
             * @param {string} type 事件类型
             * @param {Array} ids 唯一键集合
             */
            events: function ($sender, type, ids) { },
            buttons: {
                insert: {
                    show: true,
                    class: "btn btn-primary",
                    icon: "fa fa-plus",
                    text: "添加",
                    click: {
                        title: "",
                        action: ""
                    }
                },
                delete: {
                    show: true,
                    class: "btn btn-danger",
                    icon: "fa fa-trash",
                    text: "删除",
                    click: {
                        action: ""
                    }
                },
                locale: {
                    show: true,
                    class: "btn btn-warning",
                    option: ["af-ZA", "ar-SA", "ca-ES", "cs-CZ", "da-DK", "de-DE", "el-GR", "en-US", "es-AR", "es-CL", "es-CR", "es-ES", "es-MX", "es-NI", "es-SP", "et-EE", "eu-EU", "fa-IR", "fi-FI", "fr-BE", "fr-FR", "he-IL", "hr-HR", "hu-HU", "id-ID", "it-IT", "ja-JP", "ka-GE", "ko-KR", "ms-MY", "nb-NO", "nl-NL", "pl-PL", "pt-BR", "pt-PT", "ro-RO", "ru-RU", "sk-SK", "sv-SE", "th-TH", "tr-TR", "uk-UA", "ur-PK", "uz-Latn-UZ", "vi-VN", "zh-CN", "zh-TW"]           //可选择的语言列表
                },
                append: ""
            }
        },

        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        rememberOrder: true,
        headerStyle: function (column) {
            return {
                css: {
                    "text-align": "center",
                    "vertical-align": "middle"
                }
            }
        },
        pagination: true,                   //是否显示分页（*）
        paginationLoop: true,
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1,                      //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        buttonsAlign: "right",              //自带按钮对齐方式
        strictSearch: true,
        search: true,                       //是否支持分页
        showSearchButton: true,             //是否显示表格搜索，此搜索是客户端搜索，不会进服务端
        showPaginationSwitch: true,         //是否显示分页切换按钮
        showRefresh: true,                  //是否显示刷新按钮
        showToggle: true,                   //是否显示详细视图和列表视图的切换按钮
        showFullscreen: true,               //是否显示全屏按钮
        showColumns: true,                  //是否显示所有的列
        showColumnsToggleAll: true,         //是否显示切换所有
        minimumCountColumns: 2,             //最少允许的列数
        showExport: true,                   //是否显示导出数据按钮
        clickToSelect: true,                //是否启用点击选中行 
        cardView: false,                    //是否显示详细视图
        detailView: false,                  //是否显示父子表
        showLocale: true,                   //是否显示语言选项

        locale: "zh-CN",                    //当前显示语言 
        undefinedText: "",//当数据为 undefined 时显示的字符
        /**
         * 表格事件处理函数
         * @param {object} $sender 事件源
         * @param {string} type 事件类型
         * @param {Array} $row 所在行
         * @param {string} id 所在行的唯一标识
         */
        tableEvents: function ($sender, type, $row, id) { },
        columnDefaults: {
            valign: 'middle',
            sortable: true
        },
        queryParamsType: '',//查询参数组织方式
        queryParams: function (params) {//自定义参数，这里的参数是传给后台的
            return {
                pageIndex: params.pageNumber,
                pageSize: params.pageSize
            };
        }
    };

    /**
     * 初始化工具条
     */
    function initToolbar() {
        var toolbar = defaults.bsToolbar;
        if (toolbar.target === "" || $(toolbar.target).length < 1)
            return "";

        defaults.toolbar = toolbar.target;
        var $toolbar = $(toolbar.target).addClass("btn-group");

        var buttons = toolbar.buttons;
        var insert = buttons.insert;
        if (insert.show === true || $(toolbar.target).attr("data-buttons-insert") === "true") {
            $toolbar.append('<button class="' + insert.class + ' insert"><i class="' + insert.icon + '"></i>' + insert.text + '</button>');
        }
        var del = buttons.delete;
        if (del.show === true || $(toolbar.target).attr("data-buttons-del") === "true") {
            $toolbar.append('<button class="' + del.class + ' delete"><i class="' + del.icon + '"></i>' + del.text + '</button>');
        }

        var locale = buttons.locale;
        if (locale.show === true || $(toolbar.target).attr("data-buttons-locale") === "true") {
            var $select = $('<select class="' + locale.class + ' locale"></select>');
            for (var index in locale.option) {
                var option = locale.option[index];
                $select.append('<option value="' + option + '"' + (option == defaults.locale ? " selected>" : ">") + option + '</option>');
            }
            $toolbar.append($select);
        }
        $toolbar.append(buttons.append);
    }

    /**
     * 初始化列
     */
    function initColumn() {
        $.extend($.fn.bootstrapTable.columnDefaults, defaults.columnDefaults)
        var column = defaults.bsColumn;
        if (column.select.show === true) {
            defaults.columns.unshift(column.select.column);
        }
        if (column.operate.show === true) {
            var operate = column.operate.column;
            operate.formatter = "";
            var buttons = operate.buttons;
            var del = buttons.delete;
            if (del.show === true) {
                operate.formatter += "<a href='#' title='" + del.title + "' class='" + del.class + " delete'><i class='" + del.icon + "'></i>";
            }
            var upd = buttons.update;
            if (upd.show === true) {
                operate.formatter += "<a href='#' title='" + upd.title + "' class='" + upd.class + " update'><i class='" + upd.icon + "'></i>";
            }
            var dtl = buttons.detail;
            if (dtl.show === true) {
                operate.formatter += "<a href='#' title='" + dtl.title + "' class='" + dtl.class + " detail'><i class='" + dtl.icon + "'></i>";
            }
            operate.formatter += buttons.append;
            defaults.columns.push(operate);
        }
    }

    function modal(title, content) {
        return $('<div class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><div class="h4 text-center" style="margin-bottom:-2px">' + title + '</div></div><div class="modal-body">' + content + '</div></div></div>').appendTo($("body")).modal("show").on("hidden.bs.modal", function () {
            $(this).remove();
        });
    }

    function modelSuccess($modal, content) {
        $modal.on("hide.bs.modal", function () {
            $(defaults.target).bootstrapTable('refresh');
        }).find(".modal-content").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="alert-heading text-center">' + content + '</h4></div >')
    }
    function modelError($content, content, message) {
        $content.html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="alert-heading text-center">' + content + '</h4><div>' + message + '</div></div >');
    }

    function insert() {
        var modals = defaults.bsToolbar.buttons.insert.click;

        $.ajax({
            type: "get",
            url: modals.action,
            success: function (data) {
                modal(modals.title, data).find("form").submit(function () {
                    var $this = $(this);
                    var data = $this.serializeArray();
                    var post = $this.attr("action");
                    $.ajax({
                        method: "post",
                        url: post,
                        data: data,
                        success: function (res) {
                            if (res.state === true) {
                                modelSuccess($this.closest(".modal"), "添加成功")
                            } else {
                                modelError($this.closest(".modal-content"), "添加失败", res.message)
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr);
                        }
                    })
                    return false;
                });
            },
            error: function (xhr) {
                console.log(xhr);
            }
        })
    }

    $.extend({
        bsTable: function (config) {
            $.extend(true, defaults, config);

            initToolbar();
            initColumn();

            $(defaults.target).bootstrapTable(defaults);

            var toolbar = defaults.bsToolbar;
            if ($(toolbar.target).length > 0) {
                if (toolbar.buttons.locale.show === true) {
                    $(document).on("change", toolbar.target + " .locale", function () {
                        defaults.locale = $(this).val();
                        $(defaults.target).bootstrapTable('destroy');
                        $(defaults.target).bootstrapTable(defaults);
                    })
                }

                $(document).on("click", defaults.toolbar + " *, .fixed-table-toolbar *", function (e) {
                    var ids = [];
                    $.map($(defaults.target).bootstrapTable('getSelections'), function (row) {
                        var id = row[defaults.uniqueId];
                        ids.push(id);
                    })
                    if (typeof toolbar.events === "function") {
                        if ($(this).hasClass("insert")) {
                            if (toolbar.buttons.insert.click.action == "") {
                                console.log("未配置$.bsTable.bsToolbar.buttons.insert.click.action");
                            } else
                                insert();
                        }
                        else if ($(this).hasClass("delete")) {
                            if (toolbar.buttons.delete.click.action == "") {
                                console.log("未配置$.bsTable.bsToolbar.buttons.delete.click.action");
                            } else {
                                $.ajax({
                                    method: "post",
                                    url: toolbar.buttons.delete.click.action,
                                    data: { ids: ids },
                                    success: function (res) {
                                        if (res.state === true) {
                                            modelSuccess(modal("", ""), "删除成功");
                                        } else {
                                            modelError(modal("", "").find(".modal-content"), "删除失败", res.message);
                                        }
                                    },
                                    error: function (xhr) {
                                        console.log(xhr);
                                    }
                                })
                            }
                        }
                        toolbar.events($(this), e.type, ids);
                    }
                    return false;
                })
            }



            $(document).on("click dblclick", defaults.target + " *", function (e) {
                var $row = $(this).closest("tr");
                if (typeof defaults.tableEvents === "function")
                    defaults.tableEvents($(this), e.type, $row, $row.attr("data-uniqueid"));
                return false;
            })
        }
    })

})(jQuery)