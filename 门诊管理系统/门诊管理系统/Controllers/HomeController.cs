﻿using AutoMapper;
using IDAL;
using OutDepModel;
using OutDepViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace 门诊管理系统.Controllers
{
    public class HomeController : Controller
    {
        public IOutDepLoginmanage dAL { get; set; }
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger("");
        public IMapper mapper = Automapper.Configmapper();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string Name,string Pass)
        {
            
            if (Name == "")
            {
                Name = "";
                var data = 0;//如果用户未输入昵称data为0
                return Json(data);
            }
            if (Pass == "")
            {
                Pass = "";
                var data = 1;//如果用户未输入密码data为1
                return Json(data);
            }
            else//两个文本框都填写后
            {
                var name = Name;
                var pass = Pass;
                var list = dAL.Select().Where(p => p.Name== name && p.Pwd==pass).Select(p => mapper.Map<OutDepSelectModel>(p)).ToList();
                var num = list.Count > 0;//查询出该登录名的数据
                
                if (num==true)
                {
                    var role = list.First().Rool;//提取role级别
                    if (role==1)//LeveL为1data为2
                    {
                        var data = 2;
                        return Json(data);
                    }
                    if (role == 2)//LeveL为2data为3
                    {
                        var data = 3;
                        return Json(data);
                    }
                    if (role == 3)//LeveL为3data为4
                    {
                        var data = 4;
                        return Json(data);
                    }
                }
                else//如果没查询出数据
                {
                    var data = 5;
                    return Json(data);
                }
                return Json(true);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(OutDepSelectModel model)
        {
            var insert = mapper.Map<OutDepLogin>(model);
            dAL.Insert(insert);
            return RedirectToAction("Index");
        }
    }
}