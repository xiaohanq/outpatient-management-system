﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using IDAL;
using OutDepModel;
using OutDepViewModel;

namespace 门诊管理系统.Areas.Admin.Controllers
{
    public class patientController : Controller
    {
        public IOutDepadminManage dAL { get; set; }
        public IPatientmanege patientmanege { get; set; }
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger("");
        public IMapper mapper = Automapper.Configmapper();
        // GET: patient
        //主页
        public ActionResult Index(string name)
        {
            if (name == null) name = "";
            var list = patientmanege.Select().Where(p => p.Name.Contains(name)).Select(p => mapper.Map<patientselectModel>(p)).ToList();
            log.Info("用户查询了该列表");

            return View(list);
        }

        private void DropList()
        {
            var models = dAL.Select();
            List<SelectListItem> selects = new List<SelectListItem>();
            foreach (var model in models)
            {
                selects.Add(new SelectListItem() { Text = model.Name, Value = model.Name });
            }
            ViewData["OutDepName"] = selects;
        }

        public ActionResult Index2(string name)
        {
            if (name == null) name = "";
            var list = patientmanege.Select().Where(p => p.Name.Contains(name)).Select(p => mapper.Map<patientselectModel>(p)).ToList();
            log.Info("用户查询了该列表");
            return View(list);
        }
        //添加显示
        public ActionResult Create()
        {
            DropList();
            return View();
        }
        //执行添加
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(patientCreateModel model, string cmbProvince, string cmbCity, string cmbArea)
        {
            try
            {
                model.Address = cmbProvince + cmbCity + cmbArea;
                var insert = mapper.Map<Patient>(model);

                patientmanege.Insert(insert);
            }
            catch (Exception ex)
            {

                log.Error(ex.Message);
            }


            return RedirectToAction("Index");
        }
        //修改显示
        public ActionResult Edit(int? Id)
        {
            if (Id == null)
            {
                return RedirectToAction("Edit");
            }
            var temp = patientmanege.First(Id.Value);
            if (temp == null)
            {
                return RedirectToAction("Edit");
            }
            DropList();
            return View(mapper.Map<patientEditModel>(temp));
        }
        /// <summary>
        /// 执行修改
        /// </summary>
        /// <param name="moodel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]


        public ActionResult Edit(patientCreateModel moodel)
        {
            try
            {
                var Update = mapper.Map<Patient>(moodel);

                patientmanege.Update(Update);

            }
            catch (Exception ex)
            {


                log.Error(ex.Message);
            }
            return RedirectToAction("Edit");
        }

        [HttpPost]

        /// <summary>
        /// 伪删除
        /// </summary>
        /// <param name="OutDep"></param>
        /// <returns></returns>
        public ActionResult Delete(patientCreateModel OutDep)
        {
            var delete = mapper.Map<Patient>(OutDep);
            var data = patientmanege.Dleate(delete);
            return Json(data);

        }

        [HttpPost]
        public ActionResult Address()
        {
            return Json(patientmanege.Select().Select(p => p.Address));
        }
    }
}