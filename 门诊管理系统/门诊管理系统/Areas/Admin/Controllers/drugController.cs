﻿using AutoMapper;
using IDAL;
using OutDepModel;
using OutDepViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace 门诊管理系统.Areas.Admin.Controllers
{
    public class drugController : Controller
    {
        public IDrugManage dAL { get; set; }
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger("");
        public IMapper mapper = Automapper.Configmapper();
        // GET: OutDep
        /// <summary>
        /// 显示数据
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string name)
        {
            if (name == null) name = "";
            var list = dAL.Select().Where(p=>p.Name.Contains(name)).Select(p => mapper.Map<drugSelectModel>(p)).ToList();
            log.Info("用户查询了该列表");
            return View(list);
        }
        /// <summary>
        /// 添加显示
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return View();
        }
        /// <summary>
        /// 执行添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(drugCreateModel model)
        {
            try
            {
                var insert = mapper.Map<Drug>(model);
                dAL.Insert(insert);
            }
            catch (Exception)
            {

                log.Error("该页面访问错误");
            }
            return RedirectToAction("Index");
        }
        /// <summary>
        /// 修改显示
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int? Id)
        {
            if (Id == null)
            {
                return RedirectToAction("Edit");
            }
            var temp = dAL.First(Id.Value);
            if (temp == null)
            {
                return RedirectToAction("Edit");
            }
            return View(mapper.Map<drugEditModel>(temp));
        }
        /// <summary>
        ///  执行修改
        /// </summary>
        /// <param name="moodel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(drugEditModel moodel)
        {
            try
            {
                var Update = mapper.Map<Drug>(moodel);
                dAL.Update(Update);
            }
            catch (Exception)
            {

                log.Error("该页面访问错误");
            }
            return RedirectToAction("Edit");
        }
        /// <summary>
        /// 详情查询(by id)
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Details(int? Id)

        {
            var data = dAL.Select().Find(p => p.Id == Id);
            return View(mapper.Map<drugCreateModel>(data));
        }
        /// <summary>
        /// 详情显示
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        
        
        [HttpPost]

        /// <summary>
        /// 伪删除
        /// </summary>
        /// <param name="OutDep"></param>
        /// <returns></returns>
        public ActionResult Delete(drugDelteModel model)
        {
            var delete = mapper.Map<Drug>(model);
            var data= dAL.Dleate(delete);
            return Json(data);
        }
    }
}