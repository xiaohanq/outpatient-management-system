﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using IDAL;
using AutoMapper;
using OutDepViewModel;
using OutDepModel;

namespace 门诊管理系统.Areas.Admin.Controllers
{
    public class OutDepController : Controller
    {
        public IOutDepadminManage dAL { get; set; }
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger("");
        public IMapper mapper = Automapper.Configmapper();
        // GET: OutDep
        /// <summary>
        /// 显示数据
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string name)
        {
            if (name == null) name = "";
            var list = dAL.Select().Where(p => p.Name.Contains(name)).Select(p => mapper.Map<OutdepSelectModel>(p)).ToList();
            log.Info("用户查询了该列表");
            return View(list);
        }

        public ActionResult Create()
        {
            return View();
        }
        /// <summary>
        /// 执行添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OutdepSelectModel model)
        {
            try
            {
                var insert = mapper.Map<OutDepadmin>(model);

                dAL.Insert(insert);
            }
            catch (Exception ex)
            {

                log.Error(ex.Message);
            }

            return RedirectToAction("Index");
        }
        /// <summary>
        /// 修改显示
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int? Id)
        {
            if (Id == null)
            {
                return RedirectToAction("Edit");
            }
            var temp = dAL.First(Id.Value);
            if (temp == null)
            {
                return RedirectToAction("Edit");
            }
            return View(mapper.Map<OutdepEditMoodel>(temp));
        }
        /// <summary>
        ///  执行修改
        /// </summary>
        /// <param name="moodel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OutdepEditMoodel moodel)
        {
            try
            {
                var Update = mapper.Map<OutDepadmin>(moodel);
                dAL.Update(Update);
            }
            catch (Exception)
            {

                log.Error("该页面访问错误");
            }
            return RedirectToAction("Edit");
        }

        public ActionResult Details(int? Id)

        {
            var data = dAL.Select().Find(p => p.Id == Id);
            return View(mapper.Map<OutdepadCreateModel>(data));
        }



        /// <summary>
        /// 伪删除
        /// </summary>
        /// <param name="OutDep"></param>
        /// <returns></returns>
        public ActionResult Delete(OutdepEditMoodel OutDep)
        {
            ;

            var delete = mapper.Map<OutDepadmin>(OutDep);
            var data = dAL.Dleate(delete);
            return Json(data);
        }
    }
}