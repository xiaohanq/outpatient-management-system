﻿using AutoMapper;
using IDAL;
using OutDepModel;
using OutDepViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace 门诊管理系统.Areas.Admin.Controllers
{
    public class presciptController : Controller
    {
        public IPrescriptManage prescriptManage { get; set; }
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger("");
        public IMapper mapper = Automapper.Configmapper();
        // GET: prescipt
        //主页
        public ActionResult Index()
        {
            var list = prescriptManage.Select().Select(p => mapper.Map<perscriptionSelectModel>(p)).ToList();
            log.Info("用户查询了该列表");
            return View(list);
        }
        //添加显示
        public ActionResult Create()
        {
            return View();
        }
        //执行添加
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(perscriptionCreateModel model)
        {
            try
            {
                var insert = mapper.Map<Prescription>(model);

                prescriptManage.Insert(insert);
            }
            catch (Exception ex)
            {

                log.Error(ex.Message);
            }

           
            return RedirectToAction("Index");
        }
        //修改显示
        public ActionResult Edit(int? Id)
        {
            if (Id == null)
            {
                return RedirectToAction("Edit");
            }
            var temp = prescriptManage.First(Id.Value);
            if (temp == null)
            {
                return RedirectToAction("Edit");
            }
            return View(mapper.Map<perscriptionEditModel>(temp));
        }
        /// <summary>
        /// 执行修改
        /// </summary>
        /// <param name="moodel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(perscriptionCreateModel moodel)
        {
            try
            {
                var Update = mapper.Map<Prescription>(moodel);

                prescriptManage.Update(Update);
            }
            catch (Exception ex)
            {

                log.Error(ex.Message);
            }
            
            return RedirectToAction("Edit");
        }

        [HttpPost]
        /// <summary>
        /// 伪删除
        /// </summary>
        /// <param name="OutDep"></param>
        /// <returns></returns>
        public ActionResult Delete(perscriptionCreateModel OutDep)
        {
            var delete = mapper.Map<Prescription>(OutDep);
            var data = prescriptManage.Dleate(delete);
            return Json(data);
        }
    }
}