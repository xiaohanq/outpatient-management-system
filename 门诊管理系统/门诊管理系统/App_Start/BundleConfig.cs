﻿using System.Web;
using System.Web.Optimization;

namespace 门诊管理系统
{
    public class BundleConfig
    {
        // 有关捆绑的详细信息，请访问 https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                    "~/Content/bootstrap/bootstrap-{version}.css",
                    "~/Content/bootstrap-admin/bootstrap-admin-{version}.css",
                    "~/Content/bootstrap-admin/skins/_all-skins.css"));

            bundles.Add(new StyleBundle("~/Content/kindeditor").Include(
                      "~/Content/kindeditor/themes/default/default.css",
                      "~/Content/prettify/prettify.css"));

            bundles.Add(new StyleBundle("~/Content/prettify").Include(
                      "~/Content/prettify/prettify.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrapTable").Include(
                      "~/Content/bootstrap-table/bootstrap-table-{version}.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/Content/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/dbshare").Include(
                      "~/Content/extensions/bdshare.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                      "~/Content/jquery-validate/jquery-validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryExport").Include(
                      "~/Content/jquery-table-export/jquery-table-export.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                      "~/Content/modernizr/modernizr-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/bootstrap/bootstrap-{version}.js",
                      "~/Content/bootstrap-admin/bootstrap-admin-{version}.js",
                      "~/Content/bootstrap-admin/bootstrap-admin-setting.js"));

            bundles.Add(new ScriptBundle("~/bundles/kindeditor").Include(
                      "~/Content/kindeditor/kindeditor-all.js",
                      "~/Content/kindeditor/lang/zh-CN.js",
                      "~/Content/prettify/prettify.js"));

            bundles.Add(new ScriptBundle("~/bundles/prettify").Include(
                      "~/Content/prettify/prettify.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapTable").Include(
                      "~/Content/bootstrap-table/bootstrap-table-{version}.js",
                      "~/Content/bootstrap-table/bootstrap-table-locale-all.js",
                      "~/Content/bootstrap-table/bootstrap-table-export.js",
                      "~/Content/bootstrap-table/bootstrap-table-helper.js"));
        }
    }
}
